# LazyReNets

Lazy version of the reNets-datastructure, including scripts for generating the plots published in the paper.

## Guide

A simple and quick demonstration of the program can be achieved using the Eclpise IDE by passing "LazyReNets/data/sample.csv 100 10" as argument when running the Driver.

Examples for commands we executed to generate the plots can be found in scripts/exemplar_instructions.txt

## Dependencies 

To generate the paper-plots you will need to install the python package poetry (https://pypi.org/project/poetry/).


