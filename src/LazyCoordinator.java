
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class LazyCoordinator<T extends Comparable<T>> {

	public int threshold = Driver.threshold;
	public int nodes = Driver.nodes;
	public int tot_workingSet_size = 0;
	public int max_help = Driver.max_help;
	public Circ_list<T> helper_nodes;
	public HashMap<T, Node<T>> map = new HashMap<>(); // map containing all the nodes in network
	public Splay_node<T> temp;
	private StaticBTree staticTree = new StaticBTree();
	public int rout_cost = 0;
	public int topology_size = 0;
	private int constant_c;

	public void initialize(T[] ar, int constant_c) // this function take as input an array of keys from Driver and
													// initialize nodes with these keys and then add it to the map
	{
		this.constant_c = constant_c;
	}

	public void check(T x, T y) // this function just takes the src key and dst key as input and then sends it
								// to process request.
	{
		Node<T> src = this.map.get(x);
		Node<T> dst = this.map.get(y);
		process_request(src, dst);
		if (src.ego_tree != null) {
			System.out.println("Tree " + " for " + src.key);
			src.ego_tree.printTree();
		}
		System.out.println();
	}

	public void process_request(Node<T> src, Node<T> dst) // this function decides whether to answer the communication
															// request or add a new route
	{
		double start = System.nanoTime();
		if (src.status == 0) // src node is small
		{
			if (dst.status == 0) // dst node is small
			{
				if (!src.S.containsKey(dst)) // if dst not present is S, then send for add route
				{
					rout_cost += constant_c;
					System.out.println(
							"LAZY: Reached from " + src.key + "  to " + dst.key + " in " + constant_c + " hops");
				} else {
					System.out.println("LAZY: DIRECT connection from " + src.key + " to " + dst.key + " with 1 hop");
					rout_cost += 1;
				}
			} else // dst node is large
			{
				if (src.L.containsKey(dst.ego_tree)) {
					System.out.println("LAZY: Reached from " + src.key + " to root in " + constant_c
							+ " hops and then directly to " + dst.key + " in 1 hop");
					rout_cost += constant_c + 1;
				} else // if dst ego tree not present is L, then send for add route
				{
					System.out.println(
							"LAZY: Reached from " + src.key + "  to " + dst.key + " in " + constant_c + " hops");
					rout_cost += constant_c;
				}
			}
		} else // src node is large
		{
			if (dst.status == 0) // dst node is small
			{
				src.ego_tree.root.host = null;
				src.ego_tree.lazy_search(dst.key);
				if (src.ego_tree.root.key == dst.key) {
					System.out.println("LAZY: Destination found in " + src.ego_tree.hops + " hops");
					rout_cost += src.ego_tree.hops;
				} else {
					System.out.println(
							"LAZY: Reached from " + src.key + "  to " + dst.key + " in " + constant_c + " hops");
					rout_cost += constant_c;
				}
			} else // dst node is large
			{
				// find helper node
				src.ego_tree.root.host = null;
				temp = null;
				Splay_node<T> helper = src.ego_tree.lazy_search(dst.key); // whether it is unsuccessful search or not
																			// should it always splay.
				if (helper.key != dst.key) {
					System.out.println("Before finding dist, dst large");
					System.out.println(
							"LAZY: Reached from " + src.key + "  to " + dst.key + " in " + constant_c + " hops");
					rout_cost += constant_c;
				} else {
					int g = parent_traversal(helper.relay.parent);
					System.out.println("LAZY: Found the helper node( " + helper.represent
							+ " ) and jumped to another tree in 1 hop and from there to root in " + g + " hops");
					rout_cost += (g + 1);
				}
			}
			double end = System.nanoTime();
			double time_taken = end - start;
			System.out.println(time_taken);
		}
	}

	public int parent_traversal(Splay_node<T> node) {
		if (node == null) return 0;
		return 1 + parent_traversal(node.parent);
	}

	public void adopt_topology(Coordinator<T> coord) // take on new relevant values
	{
		this.tot_workingSet_size = coord.tot_workingSet_size;
		this.helper_nodes = new Circ_list<>();
		for (Entry<T, Node<T>> entry : coord.map.entrySet()) {
			this.helper_nodes.insert(entry.getKey());
		}
		for (Entry<T, Node<T>> entry : coord.map.entrySet()) { // Create new Nodes with same keys as original map
			Node<T> n = new Node<>();
			this.map.put(entry.getKey(), n);
			n.key = entry.getKey();
		}
		// update map with new entries
		for (Entry<T, Node<T>> entry : this.map.entrySet()) {
			ArrayList<Node<T>> original_working = coord.map.get(entry.getKey()).working_set; // take original workingset
																								// of current Key
			for (Node<T> a : original_working) {
				entry.getValue().working_set.add(this.map.get(a.key));
			}
		}
		for (Entry<T, Node<T>> entry : this.map.entrySet()) {
			HashMap<Node<T>, Integer> original_S = coord.map.get(entry.getKey()).S; // take original set S of current
																					// Key
			for (Entry<Node<T>, Integer> entry2 : original_S.entrySet()) { // iterate through original set
				entry.getValue().S.put(entry2.getKey(), 1); // put correct node into new set of S
			}
		}
		for (Entry<T, Node<T>> entry : this.map.entrySet()) {
			HashMap<splayTree<T>, Splay_node<T>> original_L = coord.map.get(entry.getKey()).L; // take original set L of
																								// current Key
			for (Entry<splayTree<T>, Splay_node<T>> entry2 : original_L.entrySet()) { // iterate through original set
				entry.getValue().L.put(entry2.getKey(), entry2.getValue()); // put correct nodes into new set of trees L
			}
		}
		for (Entry<T, Node<T>> entry : this.map.entrySet()) {
			Node<T> n = entry.getValue();
			if (coord.map.get(entry.getKey()).status == 1 && n.status == 0) {
				lazyMakeLarge(n);
			} else if (n.status == 1 && entry.getValue().status == 0) {
				n.ego_tree = null;
				n.status = 0;
			}
			n.num_helps = entry.getValue().num_helps;
		}

		// calculate topology size (ego trees links + small node links)
		for (int i = 1; i <= map.size(); i++) {
			Node<T> n = map.get(i);
			topology_size += n.S.size() / 2; // division by 2 because links are double stored (src-dst + dst-src)
			if (n.status == 1) {
				topology_size += (n.ego_tree.size - 1); // n-1 links for ego tree
			}
		}
	}

	public void lazyMakeLarge(Node<T> n) {
		n.status = 1;
		splayTree<T> ego_tree = new splayTree<>();
		int x = 0;
		ArrayList<Node<T>> large = new ArrayList<>();
		for (int i = 0; i <= n.working_set.size() - 1; i++) // starting to add the keys from working set and also
															// performing splaying operation(to kind of maintain
															// balances tree)
		{
			// only adding small nodes to the tree
			if (n.working_set.get(i).status == 0 && x == 0) {
				ego_tree.set_root(n.working_set.get(i).key);
				x++;
			} else if (n.working_set.get(i).status == 0 && x != 0) {
				Splay_node<T> h = ego_tree.insert(n.working_set.get(i).key);
			} else if (n.working_set.get(i).status == 1) {
				n.working_set.get(i).ego_tree.root.host = null;
				n.working_set.get(i).ego_tree.root.host = n.working_set.get(i);
				large.add(n.working_set.get(i)); // storing all large nodes in a list
			}
		}
		n.ego_tree = ego_tree;
		n.ego_tree.root.host = n;
		for (int i = 0; i < large.size(); i++) {
			Splay_node<T>[] arr = find_helper_node(n, large.get(i));
			if (arr == null) // this means the network is reset
				return;
		}
	}

	public Splay_node<T>[] find_helper_node(Node<T> node1, Node<T> node2) {
		if (helper_nodes.size == 0) // if there are no helper nodes, then original network would be reset
		{
			return null;
		}
		T helper = helper_nodes.get();
		System.out.println(node1.key + " " + node2.key + " " + helper);
		map.get(helper).num_helps++;
		if (map.get(helper).num_helps == max_help) helper_nodes.remove(helper);
		if (node1.ego_tree.root.key == null) {
			node1.ego_tree.set_root(node1.key);
		}
		Splay_node<T> tr1 = node1.ego_tree.insert(node2.key);
		Splay_node<T> tr2 = node2.ego_tree.insert(node1.key);
		tr1.represent = helper;
		tr2.represent = helper;
		tr1.relay = tr2;
		tr2.relay = tr1;
		Splay_node<T>[] arr = new Splay_node[2];
		arr[0] = tr1;
		arr[1] = tr2;
		return arr;
	}

	public void inorder(Splay_node<T> node, ArrayList<Splay_node<T>> ar) {
		if (node == null) return;
		inorder(node.left, ar);
		ar.add(node);
		inorder(node.right, ar);
	}
}