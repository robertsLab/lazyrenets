/*
 * Code taken from https://www.sanfoundry.com/java-program-create-balanced-binary-tree-incoming-data/
 * 	
 */

public class StaticBTree<T extends Comparable<T>> {

	class SBBSTNodes {

		SBBSTNodes left, right;
		T data;
		int height;

		public SBBSTNodes() {
			left = null;
			right = null;
			height = 0;
		}

		public SBBSTNodes(int n) {
			left = null;
			right = null;
			height = 0;
		}

		public SBBSTNodes(T ar) {
			left = null;
			right = null;
			data = ar;
			height = 0;
		}
	}

	public SBBSTNodes root;
	public int searchHops = 0;
	// Global static variable
	static int d1 = -1;
	static int d2 = -1;
	static int dist = 0;

	public StaticBTree() {
		root = null;
	}

	public T get_root_data() {
		return root.data;
	}

	public boolean isEmpty() {
		return root == null;
	}

	public void clear() {
		root = null;
	}

	public void insert(T n) {
		root = insert(n, root);
	}

	private int height(SBBSTNodes t) {
		return t == null ? -1 : t.height;
	}

	private int max(int lhs, int rhs) {
		return lhs > rhs ? lhs : rhs;
	}

	private SBBSTNodes insert(T x, SBBSTNodes t) {
		if (t == null)
			t = new SBBSTNodes(x);
		else if (x.compareTo(t.data) < 0) {
			t.left = insert(x, t.left);
			if (height(t.left) - height(t.right) == 2) if (x.compareTo(t.left.data) < 0)
				t = rotateWithLeftChild(t);
			else
				t = doubleWithLeftChild(t);
		} else if (x.compareTo(t.data) > 0) {
			t.right = insert(x, t.right);
			if (height(t.right) - height(t.left) == 2) if (x.compareTo(t.right.data) > 0)
				t = rotateWithRightChild(t);
			else
				t = doubleWithRightChild(t);
		} else
			;
		t.height = max(height(t.left), height(t.right)) + 1;
		return t;
	}

	private SBBSTNodes rotateWithLeftChild(SBBSTNodes k2) {
		SBBSTNodes k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		k2.height = max(height(k2.left), height(k2.right)) + 1;
		k1.height = max(height(k1.left), k2.height) + 1;
		return k1;
	}

	private SBBSTNodes rotateWithRightChild(SBBSTNodes k1) {
		SBBSTNodes k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		k1.height = max(height(k1.left), height(k1.right)) + 1;
		k2.height = max(height(k2.right), k1.height) + 1;
		return k2;
	}

	private SBBSTNodes doubleWithLeftChild(SBBSTNodes k3) {
		k3.left = rotateWithRightChild(k3.left);
		return rotateWithLeftChild(k3);
	}

	private SBBSTNodes doubleWithRightChild(SBBSTNodes k1) {
		k1.right = rotateWithLeftChild(k1.right);
		return rotateWithRightChild(k1);
	}

	public int countNodes() {
		return countNodes(root);
	}

	private int countNodes(SBBSTNodes r) {
		if (r == null)
			return 0;
		else {
			int l = 1;
			l += countNodes(r.left);
			l += countNodes(r.right);
			return l;
		}
	}

	public boolean search(T val) {
		return search(root, val);
	}

	public boolean search(SBBSTNodes r, T val) {
		boolean found = false;
		while ((r != null) && !found) {
			T rval = r.data;
			if (val.compareTo(rval) < 0)
				r = r.left;
			else if (val.compareTo(rval) > 0)
				r = r.right;
			else {
				found = true;
				break;
			}
			++searchHops;
			found = search(r, val);
		}
		if (found == false) searchHops = 0; // reset hops
		return found;
	}

	public void inorder() {
		inorder(root);
	}

	private void inorder(SBBSTNodes r) {
		if (r != null) {
			inorder(r.left);
			System.out.print(r.data + " ");
			inorder(r.right);
		}
	}

	public void preorder() {
		preorder(root);
	}

	private void preorder(SBBSTNodes r) {
		if (r != null) {
			System.out.print(r.data + " ");
			preorder(r.left);
			preorder(r.right);
		}
	}

	public void postorder() {
		postorder(root);
	}

	private void postorder(SBBSTNodes r) {
		if (r != null) {
			postorder(r.left);
			postorder(r.right);
			System.out.print(r.data + " ");
		}
	}

	// code from
	// https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/
	// Returns level of key k if it is present
	// in tree, otherwise returns -1
	int findLevel(SBBSTNodes root, T k, int level) {

		// Base Case
		if (root == null) {
			return -1;
		}

		// If key is present at root, or in left
		// subtree or right subtree, return true;
		if (root.data == k) {
			return level;
		}

		int l = findLevel(root.left, k, level + 1);
		return (l != -1) ? l : findLevel(root.right, k, level + 1);
	}

	// This function returns pointer to LCA of
	// two given values n1 and n2. It also sets
	// d1, d2 and dist if one key is not ancestor of other
	// d1 -. To store distance of n1 from root
	// d2 -. To store distance of n2 from root
	// lvl -. Level (or distance from root) of current node
	// dist -. To store distance between n1 and n2
	public StaticBTree.SBBSTNodes findDistUtil(SBBSTNodes root, T n1, T n2, int lvl) {

		// Base case
		if (root == null) {
			return null;
		}

		// If either n1 or n2 matches with root's
		// key, report the presence by returning
		// root (Note that if a key is ancestor of
		// other, then the ancestor key becomes LCA
		if (root.data == n1) {
			d1 = lvl;
			return root;
		}
		if (root.data == n2) {
			d2 = lvl;
			return root;
		}

		// Look for n1 and n2 in left and right subtrees
		SBBSTNodes left_lca = findDistUtil(root.left, n1, n2, lvl + 1);
		SBBSTNodes right_lca = findDistUtil(root.right, n1, n2, lvl + 1);

		// If both of the above calls return Non-null,
		// then one key is present in once subtree and
		// other is present in other, So this node is the LCA
		if (left_lca != null && right_lca != null) {
			dist = (d1 + d2) - 2 * lvl;
			return root;
		}

		// Otherwise check if left subtree
		// or right subtree is LCA
		return (left_lca != null) ? left_lca : right_lca;
	}

	// The main function that returns distance
	// between n1 and n2. This function returns -1
	// if either n1 or n2 is not present in
	// Binary Tree.
	public int findDistance(SBBSTNodes root, T n1, T n2) {
		d1 = -1;
		d2 = -1;
		dist = 0;
		SBBSTNodes lca = findDistUtil(root, n1, n2, 1);

		// If both n1 and n2 were present
		// in Binary Tree, return dist
		if (d1 != -1 && d2 != -1) {
			return dist;
		}

		// If n1 is ancestor of n2, consider
		// n1 as root and find level
		// of n2 in subtree rooted with n1
		if (d1 != -1) {
			dist = findLevel(lca, n2, 0);
			return dist;
		}

		// If n2 is ancestor of n1, consider
		// n2 as root and find level
		// of n1 in subtree rooted with n2
		if (d2 != -1) {
			dist = findLevel(lca, n1, 0);
			return dist;
		}
		return -1;
	}
}
