import java.io.BufferedReader;
import java.io.FileReader;

public class Driver {

	public static Coordinator<Integer> coord;
	public static LazyCoordinator<Integer> lazyCoord;
	public static int threshold = 4;
	public static int max_help = 1;
	public static int nodes;
	public static int total_cost = 0;
	public static int epoch_cost = 0;
	public static int alpha = 12;
	public static int constant_c = 6;

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			System.out.println("Usage: Driver <path to csv> <n> <alpha>");
			return;
		}

		nodes = Integer.parseInt(args[1]) + 1;
		alpha = Integer.parseInt(args[2]);

		coord = new Coordinator<>(); // cached topology
		lazyCoord = new LazyCoordinator<>(); // physical topology

		// Initialization of nodes
		Integer[] arr = new Integer[nodes + 1];
		for (int i = 1; i <= nodes; i++) {
			arr[i] = i;
		}
		String s;
		coord.initialize(arr, constant_c);
		lazyCoord.initialize(arr, constant_c);
		lazyCoord.adopt_topology(coord); // adopt same topology at beginning

		int count = 0;
		int num_seq = nodes;
		BufferedReader br2 = new BufferedReader(new FileReader(args[0]));

		br2.readLine(); // skip header of csv
		while ((s = br2.readLine()) != null && num_seq > 0) {
			--num_seq;
			String[] pair = s.split(",");
			int x = Integer.parseInt(pair[0]) + 1;
			int y = Integer.parseInt(pair[1]) + 1;
			System.out.println("X = " + x + " Y=" + y);

			coord.check(x, y);
			lazyCoord.check(x, y);
			total_cost += lazyCoord.rout_cost;
			epoch_cost += lazyCoord.rout_cost;
			lazyCoord.rout_cost = 0;
			if (epoch_cost >= alpha) { // synchronize physical to cached
				lazyCoord.adopt_topology(coord);
				epoch_cost = 0;
				total_cost += alpha;
				++count;
			}
		}
		System.out.println("Physical (LazyReNet): Total cost = " + total_cost + "." + "\nPhysical topology changed "
				+ count + " times. Alpha param = " + alpha + "\nDynamic (ReNet): Total routing cost = "
				+ coord.route_cost + ". Total adjusting cost = " + coord.adj_cost);

		System.out.println(
				(total_cost - alpha * count) + "," + (alpha * count) + "," + coord.route_cost + "," + coord.adj_cost);
		System.out.println("Total items served: " + coord.inserted_items.size());
		br2.close();
	}

}