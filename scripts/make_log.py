import argparse
import pathlib
import subprocess
import logging
import csv
import pandas as pd
import sys

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

def java_exec(cp, module, args):
    result = ["/home/robert/.p2/pool/plugins/org.eclipse.justj.openjdk.hotspot.jre.full.linux.x86_64_15.0.2.v20210201-0955/jre/bin/java",
            "-classpath",
            cp,
            module, *args]

    logger.info("Running: " + " ".join(result))
    return result


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--bin-path", required=True,
                        help="Path to the compiled classes for add it to the java classpath")
    parser.add_argument("--requests", "-r", nargs="+", help="Path to the input csv file with requests")

    parser.add_argument("--alpha-start", required=True, type=int)
    parser.add_argument("--alpha-step", required=True, type=int)
    parser.add_argument("--alpha-end", required=True, type=int)

    parser.add_argument("--log", required=True, help="Path to the log file")

    args = parser.parse_args()

    res = pd.DataFrame(
        columns=["alpha", "Sequence", "routeReNet", "adjReNet", "routeLazy", "adjLazy", "Ratio of LazyReNet to ReNet"])

    for req_path in args.requests:
        nodes_number = 0
        with open(req_path) as requests_f:
            requests_csv = csv.DictReader(requests_f)
            for row in requests_csv:
                nodes_number = max(nodes_number, int(row["src"]))
                nodes_number = max(nodes_number, int(row["dst"]))


        for alpha in range(args.alpha_start, args.alpha_end + 1, args.alpha_step):
            output = subprocess.check_output(
                java_exec(args.bin_path, "Driver", [req_path, str(1000), str(alpha)]))
            output = (output.splitlines()[-2]).decode("utf-8")  # take last line of output with needed statistic
            output = [alpha] + [int(x) for x in output.split(",")]

            res = res.append({"alpha": alpha,
                              "routeReNet": output[3], "adjReNet": output[4],
                              "routeLazy": output[1], "adjLazy": output[2], "Sequence": pathlib.Path(req_path).stem,
                              "Ratio of Lazy ReNet to ReNet": (output[1] + output[2]) / (output[3] + output[4])},
                             ignore_index=True)

        res.to_csv(args.log)


if __name__ == '__main__':
    main()
