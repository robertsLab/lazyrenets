import argparse
import pandas as pd
import plotly.express as px

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--traces", action="append", help="Name of trace")
parser.add_argument("-x", default="alpha", help="Name of x axis and data source column")
parser.add_argument("-y", default="Ratio of Lazy ReNet to ReNet", help="Name of y axis and data source column")

args = parser.parse_args()

res_df = pd.DataFrame()

for trace in args.traces:
    data = pd.read_csv(trace)

    res_df = res_df.append(data, ignore_index=True)
print(res_df)
fig = px.line(res_df, x=args.x, y=args.y, color="Sequence")
fig.update_layout(
    font_family="Times New Roman",
    font_color="black",
    font_size=20,
    xaxis_title="$\\alpha$",
    legend_orientation="h",
    legend_x=0.1,
    legend_y=-0.15,
    legend_title=""
)
fig.write_image(f"merged.pdf")

